/*

Теоретичні запитання:

  1.Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.

    Вони допомогають ефективно обробляти великі об'єми даних, налаштовувати правила відбору,
    та форму представлення інформації. Також дозволяють створити обмеження коректного виконання
    якогось етапа для перехіду к іншому.

  2.Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.

   for - для створення дій для можливиж діапазонів (вікові групи);
   while - валідація даннх користувача (тільки після коректного заповнення імені та віку можливо вийти з циклу);
   
      
  3.Що таке явне та неявне приведення (перетворення) типів даних у JS?

   явне - оголошення використання або цільове використання конвертації, наприклад: String(value);
   неявне - використання можливостей мови (слабка типізація) для автоматичної зміни типу даних, наприклад: 0 == false, 1 + "Hello, world!" - string 1Hello, world!

*/

let userNumber;

do {userNumber = +prompt("Add Your number", userNumber ? null :'')

} while (!Number.isInteger(userNumber) || userNumber === 0); //Додана перевірка цілого числа

for (i = 1; i<=+userNumber; i++) {
  
  if (userNumber < 5) {
    console.log("Sorry, no numbers");
    break;
      
  } else if (i % 5 === 0) {
    console.log(i);

  } 
}

/* Друге завдання з m/n */

let numberOne = prompt('Add first number');
let numberTwo = prompt('Add second number');

let leftNumber  = (numberOne < numberTwo) ? numberOne :  numberTwo;
let rightNumber = (numberOne > numberTwo) ? numberOne :  numberTwo;

console.log(`The range is from ${leftNumber} to ${rightNumber}`);

for( i = leftNumber; i <= rightNumber; i++) {

  for(j = 2; j < i; j++) {

    if(i % j === 0) {
      break;

    }
  }

  if(i === j)

  console.log(i);

}